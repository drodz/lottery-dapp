# lottery-dapp
Simple lottery Ethereum dapp created during the Udemy course "Ethereum and Solidity: The Complete Developer's Guide" (https://eylearning.udemy.com/course/ethereum-and-solidity-the-complete-developers-guide/learn/)

## How to run
Run:
```
yarn install
```

Export following environment variables:

| Key                          | Value                                |
| ---------------------------- | ------------------------------------ |
| `SECRET_MNEMONIC`            | Mnemonic for your Ethereum wallet    |
| `INFURA_URL`                 | A link to Infura node                |

Compile contract and deploy it to the blockchain:
```
yarn run ethereum:compile
yarn run ethereum:deploy
```

Use deployed contract's address and set it to an environment variable:

| Key                          | Value                                |
| ---------------------------- | ------------------------------------ |
| `LOTTERY_CONTRACT_ADDRESS`   | Address of deployed contract         |

Run the application:
```
yarn run
```
