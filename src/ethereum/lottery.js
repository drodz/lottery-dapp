import web3 from "./web3";
import Lottery from './build/Lottery'

export default new web3.eth.Contract(
  JSON.parse(Lottery.interface),
  process.env.REACT_APP_LOTTERY_CONTRACT_ADDRESS
);
